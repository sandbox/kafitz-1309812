
README.txt
==========

Technical
=========
This is the oneshare module package 7.x, and will work with drupal 7.x

Explanation
===========
This module is an integration of the addthis functionality. So it is possible
to share content to different social media. You have the choice to use the
standard addthis button, or use the toolbox where you can select a predefined
amount of social media where you can share with, or customize your buttons
for sharing with other social media.

Current Features
================
* Integration of the addthis standard button
* Integration of the addthis toolbox.
* Possibility to assign content sharing on node types.
* There is also a addthis block widget.

Future Features
===============
* Add your own social media buttons for content sharing.
* Integration of google+ and facebook like.
* Integration of facebook meta tags.
