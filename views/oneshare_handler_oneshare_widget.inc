<?php

/**
 * @file
 * Field handler to display the oneshare widget.
 */
class oneshare_handler_oneshare_widget extends views_handler_area {
  function option_definition() {
    $options = parent::option_definition();
    $options['oneshare_widget'] = array('default' => FALSE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $profiles = _oneshare_load_profiles();
    $profile_select = array();

    foreach ($profiles as $key => $profile) {
      $profile_select[$profile->type] = $profile->name;
    }

    $form['oneshare_widget'] = array(
      '#type' => 'select',
      '#title' => t('Select your profile'),
      '#options' => $profile_select,
      '#default_value' => $this->options['oneshare_widget'],
    );
  }

  function options_submit(&$form, &$form_state) {
    $form_state['values']['options']['oneshare_widget'] = $form_state['values']['options']['oneshare_widget'];
    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {
    $profile = _oneshare_load_profiles($this->options['oneshare_widget']);
    return $this->render_oneshare_widget($profile);
  }

  function render_oneshare_widget($profile) {
    return oneshare_addthis_widget($profile);
  }
}
