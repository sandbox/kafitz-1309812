<?php
/**
 * @file
 * Views integration for Oneshare.
 */

/**
 * Implementation of hook_views_data().
 */
function oneshare_views_data() {
  $data = array();

  // Oneshare widget.
  $data['node']['oneshare'] = array(
    'title' => t('Oneshare widget'),
    'help' => t('Provides the oneshare widget.'),
    'area' => array(
      'handler' => 'oneshare_handler_oneshare_widget',
    ),
  );

  return $data;
}
