<?php
/**
 * @file
 * Container theme implementation for displaying the standerd addthis button
 * or the toolbox, with a preferred amount of buttons or with user configured
 * buttons.
 */
?>
<div class="social-media-plugin" id="<?php print $widget_id; ?>">
  <div class="addthis_<?php print $type; ?> addthis_default_style" addthis:url="" addthis:description="">
    <?php print $output; ?>
  </div>
</div>

