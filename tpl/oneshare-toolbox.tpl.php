<?php
/**
 * @file
 * Theme implementation for displaying a toolbox with a certain amount
 * of preferred buttons.
 */
?>
<?php for($x = 1; $x <= $pref_amount; $x++): ?>
  <a class="addthis_button_preferred_<?php print $x; ?> addthis_<?php print $image_size; ?>_style"></a>
<?php endfor; ?>
<span class="addthis_separator">|</span>
<a class="addthis_button_compact addthis_<?php print $image_size; ?>_style"></a>

