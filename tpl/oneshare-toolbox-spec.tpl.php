<?php
/**
 * @file
 * Theme implementation for displaying a toolbox with user configured
 * buttons.
 */
?>
<?php foreach ($buttons_order as $key => $button): ?>
  <?php if ($buttons[$button]['display']): ?>
    <?php if ($button == 'seperator'): ?>
      <span class="addthis_separator">
        <?php print t($buttons[$button]['label']); ?>
      </span>
    <?php else: ?>
      <a class="addthis_button_<?php print $button; ?> addthis_<?php print $image_size; ?>_style">
        <?php print t($buttons[$button]['label']); ?>
      </a>
    <?php endif; ?>
  <?php endif; ?>
<?php endforeach; ?>

