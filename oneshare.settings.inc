<?php
/**
 * @file
 * Settings file for oneshare module.
 */

/**
 * Returns an array of all used custom toolbox buttons and it's settings.
 */
function oneshare_toolbox_buttons() {
  return array(
    'facebook' => array(
      'label' => '',
      'display' => TRUE,
      'del' => FALSE,
    ),
    'twitter' => array(
      'label' => '',
      'display' => TRUE,
      'del' => FALSE,
    ),
    'netlog' => array(
      'label' => '',
      'display' => FALSE,
      'del' => FALSE,
    ),
    'myspace' => array(
      'label' => '',
      'display' => TRUE,
      'del' => FALSE,
    ),
    'email' => array(
      'label' => '',
      'display' => FALSE,
      'del' => FALSE,
    ),
    'print' => array(
      'name' => 'print',
      'label' => '',
      'display' => FALSE,
      'del' => FALSE,
    ),
    'seperator' => array(
      'label' => '|',
      'display' => TRUE,
      'del' => FALSE,
    ),
    'expanded' => array(
      'label' => '',
      'display' => FALSE,
      'del' => FALSE,
    ),
  );
}

/**
 * Returns an array of social media that could be used as a custom toolbox
 * button.
 */
function oneshare_newsocialmedia() {
  return array(
    '100zakladok' => array(
      'label' => 'Bookmarking Site'
    ),
    '2tag' => array(
      'label' => 'Tools'
    ),
    '2linkme' => array(
      'label' => 'Bookmarking Site'
    ),
    '7live7' => array(
      'label' => 'Bookmarking Site'
    ),
    'a97abi' => array(
      'label' => 'Social Network'
    ),
    'addressbar' => array(
      'label' => 'Other'
    ),
    'menu' => array(
      'label' => 'Tools'
    ),
    'adfty' => array(
      'label' => 'Social News'
    ),
    'adifni' => array(
      'label' => 'Bookmarking Site'
    ),
    'amazonwishlist' => array(
      'label' => 'Shopping Site'
    ),
    'amenme' => array(
      'label' => 'Social Network'
    ),
    'aim' => array(
      'label' => 'Blogging Platform'
    ),
    'aolmail' => array(
      'label' => 'Email/IM Service'
    ),
    'armenix' => array(
      'label' => 'Social Network'
    ),
    'arto' => array(
      'label' => 'Social Network'
    ),
    'aviary' => array(
      'label' => 'Tools'
    ),
    'baang' => array(
      'label' => 'Social News'
    ),
    'baidu' => array(
      'label' => 'Social Network'
    ),
    'bebo' => array(
      'label' => 'Social Network'
    ),
    'biggerpockets' => array(
      'label' => 'Social Network'
    ),
    'bitly' => array(
      'label' => 'Tools'
    ),
    'bizsugar' => array(
      'label' => 'Social News'
    ),
    'blinklist' => array(
      'label' => 'Bookmarking Site'
    ),
    'blip' => array(
      'label' => 'Blogging Platform'
    ),
    'blogger' => array(
      'label' => 'Blogging Platform'
    ),
    'bloggy' => array(
      'label' => 'Blogging Platform'
    ),
    'blogmarks' => array(
      'label' => 'Bookmarking Site'
    ),
    'blogtrottr' => array(
      'label' => 'Tools'
    ),
    'blurpalicious' => array(
      'label' => 'Bookmarking Site'
    ),
    'bobrdobr' => array(
      'label' => 'Bookmarking Site'
    ),
    'bonzobox' => array(
      'label' => 'Bookmarking Site'
    ),
    'socialbookmarkingnet' => array(
      'label' => 'Bookmarking Site'
    ),
    'bookmarkycz' => array(
      'label' => 'Bookmarking Site'
    ),
    'bookmerkende' => array(
      'label' => 'Bookmarking Site'
    ),
    'bordom' => array(
      'label' => 'Social News'
    ),
    'box' => array(
      'label' => 'Tools'
    ),
    'brainify' => array(
      'label' => 'Bookmarking Site'
    ),
    'bryderi' => array(
      'label' => 'Shopping Site'
    ),
    'buddymarks' => array(
      'label' => 'Bookmarking Site'
    ),
    'buzzzy' => array(
      'label' => 'Social Network'
    ),
    'camyoo' => array(
      'label' => 'Social Network'
    ),
    'cardthis' => array(
      'label' => 'Tools'
    ),
    'care2' => array(
      'label' => 'Social Network'
    ),
    'chiq' => array(
      'label' => 'Shopping Site'
    ),
    'cirip' => array(
      'label' => 'Blogging Platform'
    ),
    'citeulike' => array(
      'label' => 'Bookmarking Site'
    ),
    'classicalplace' => array(
      'label' => 'Social Network'
    ),
    'cndig' => array(
      'label' => 'Social Network'
    ),
    'colivia' => array(
      'label' => 'Social News'
    ),
    'technerd' => array(
      'label' => 'Social Network'
    ),
    'connotea' => array(
      'label' => 'Bookmarking Site'
    ),
    'cootopia' => array(
      'label' => 'Bookmarking Site'
    ),
    'cosmiq' => array(
      'label' => 'Bookmarking Site'
    ),
    'curateus' => array(
      'label' => 'Tools'
    ),
    'delicious' => array(
      'label' => 'Bookmarking Site'
    ),
    'designbump' => array(
      'label' => 'Bookmarking Site'
    ),
    'digthiswebhost' => array(
      'label' => 'Social News'
    ),
    'digaculturanet' => array(
      'label' => 'Social News'
    ),
    'digg' => array(
      'label' => 'Social News'
    ),
    'diggita' => array(
      'label' => 'Social News'
    ),
    'digo' => array(
      'label' => 'Social News'
    ),
    'digzign' => array(
      'label' => 'Social News'
    ),
    'diigo' => array(
      'label' => 'Social Network'
    ),
    'dipdive' => array(
      'label' => 'Social Network'
    ),
    'domelhor' => array(
      'label' => 'Social News'
    ),
    'dosti' => array(
      'label' => 'Social Network'
    ),
    'dotnetkicks' => array(
      'label' => 'Social News'
    ),
    'dotnetshoutout' => array(
      'label' => 'Social News'
    ),
    'douban' => array(
      'label' => 'Social Network'
    ),
    'draugiem' => array(
      'label' => 'Social Network'
    ),
    'drimio' => array(
      'label' => 'Social Network'
    ),
    'dropjack' => array(
      'label' => 'Social News'
    ),
    'dzone' => array(
      'label' => 'Bookmarking Site'
    ),
    'edelight' => array(
      'label' => 'Shopping Site'
    ),
    'efactor' => array(
      'label' => 'Social Network'
    ),
    'ekudos' => array(
      'label' => 'Social News'
    ),
    'elefantapl' => array(
      'label' => 'Bookmarking Site'
    ),
    'mailto' => array(
      'label' => 'Email/IM Service'
    ),
    'embarkons' => array(
      'label' => 'Social Network'
    ),
    'eucliquei' => array(
      'label' => 'Bookmarking Site'
    ),
    'evernote' => array(
      'label' => 'Other'
    ),
    'extraplay' => array(
      'label' => 'Social Network'
    ),
    'ezyspot' => array(
      'label' => 'Bookmarking Site'
    ),
    'stylishhome' => array(
      'label' => 'Shopping Site'
    ),
    'fabulously40' => array(
      'label' => 'Social Network'
    ),
    'facebook_like' => array(
      'label' => 'Social Network'
    ),
    'informazione' => array(
      'label' => 'Social News'
    ),
    'fark' => array(
      'label' => 'Social News'
    ),
    'farkinda' => array(
      'label' => 'Social News'
    ),
    'fashiolista' => array(
      'label' => 'Shopping Site'
    ),
    'fashionburner' => array(
      'label' => 'Shopping Site'
    ),
    'favable' => array(
      'label' => 'Bookmarking Site'
    ),
    'faves' => array(
      'label' => 'Bookmarking Site'
    ),
    'favlogde' => array(
      'label' => 'Bookmarking Site'
    ),
    'favoritende' => array(
      'label' => 'Bookmarking Site'
    ),
    'favorites' => array(
      'label' => 'Bookmarking Site'
    ),
    'favoritus' => array(
      'label' => 'Bookmarking Site'
    ),
    'flaker' => array(
      'label' => 'Social Network'
    ),
    'flosspro' => array(
      'label' => 'Blogging Platform'
    ),
    'folkd' => array(
      'label' => 'Bookmarking Site'
    ),
    'formspring' => array(
      'label' => 'Social Network'
    ),
    'thefreedictionary' => array(
      'label' => 'Bookmarking Site'
    ),
    'fresqui' => array(
      'label' => 'Social News'
    ),
    'friendfeed' => array(
      'label' => 'Social Network'
    ),
    'funp' => array(
      'label' => 'Bookmarking Site'
    ),
    'fwisp' => array(
      'label' => 'Bookmarking Site'
    ),
    'gabbr' => array(
      'label' => 'Social News'
    ),
    'gamekicker' => array(
      'label' => 'Social News'
    ),
    'givealink' => array(
      'label' => 'Bookmarking Site'
    ),
    'globalgrind' => array(
      'label' => 'Social Network'
    ),
    'gmail' => array(
      'label' => 'Email/IM Service'
    ),
    'govn' => array(
      'label' => 'Social Network'
    ),
    'godudu' => array(
      'label' => 'Social Network'
    ),
    'goodnoows' => array(
      'label' => 'Social News'
    ),
    'google' => array(
      'label' => 'Bookmarking Site'
    ),
    'google_plusone' => array(
      'label' => 'Social Network'
    ),
    'googletranslate' => array(
      'label' => 'Tools'
    ),
    'greaterdebater' => array(
      'label' => 'Social News'
    ),
    'grono' => array(
      'label' => 'Social Network'
    ),
    'habergentr' => array(
      'label' => 'Social News'
    ),
    'hackernews' => array(
      'label' => 'Social News'
    ),
    'hadashhot' => array(
      'label' => 'Social News'
    ),
    'hatena' => array(
      'label' => 'Bookmarking Site'
    ),
    'gluvsnap' => array(
      'label' => 'Social News'
    ),
    'hedgehogs' => array(
      'label' => 'Social Network'
    ),
    'hellotxt' => array(
      'label' => 'Blogging Platform'
    ),
    'historious' => array(
      'label' => 'Bookmarking Site'
    ),
    'hotbookmark' => array(
      'label' => 'Bookmarking Site'
    ),
    'hotklix' => array(
      'label' => 'Social News'
    ),
    'hotmail' => array(
      'label' => 'Email/IM Service'
    ),
    'w3validator' => array(
      'label' => 'Tools'
    ),
    'hyves' => array(
      'label' => 'Social Network'
    ),
    'identica' => array(
      'label' => 'Blogging Platform'
    ),
    'igoogle' => array(
      'label' => 'Other'
    ),
    'ihavegot' => array(
      'label' => 'Social Network'
    ),
    'index4' => array(
      'label' => 'Bookmarking Site'
    ),
    'indexor' => array(
      'label' => 'Bookmarking Site'
    ),
    'instapaper' => array(
      'label' => 'Tools'
    ),
    'investorlinks' => array(
      'label' => 'Social News'
    ),
    'iorbix' => array(
      'label' => 'Social Network'
    ),
    'society' => array(
      'label' => 'Social Network'
    ),
    'iwiw' => array(
      'label' => 'Social Network'
    ),
    'jamespot' => array(
      'label' => 'Bookmarking Site'
    ),
    'jappy' => array(
      'label' => 'Social Network'
    ),
    'joliprint' => array(
      'label' => 'Tools'
    ),
    'jumptags' => array(
      'label' => 'Bookmarking Site'
    ),
    'kaboodle' => array(
      'label' => 'Shopping Site'
    ),
    'kaevur' => array(
      'label' => 'Bookmarking Site'
    ),
    'kaixin' => array(
      'label' => 'Social Network'
    ),
    'ketnooi' => array(
      'label' => 'Social Network'
    ),
    'kindleit' => array(
      'label' => 'Tools'
    ),
    'kipup' => array(
      'label' => 'Social Network'
    ),
    'kledy' => array(
      'label' => 'Bookmarking Site'
    ),
    'kommenting' => array(
      'label' => 'Social News'
    ),
    'latafaneracat' => array(
      'label' => 'Bookmarking Site'
    ),
    'librerio' => array(
      'label' => 'Bookmarking Site'
    ),
    'linkninja' => array(
      'label' => 'Bookmarking Site'
    ),
    'linkagogo' => array(
      'label' => 'Bookmarking Site'
    ),
    'linkedin' => array(
      'label' => 'Social Network'
    ),
    'linksgutter' => array(
      'label' => 'Bookmarking Site'
    ),
    'linkshares' => array(
      'label' => 'Bookmarking Site'
    ),
    'linkuj' => array(
      'label' => 'Social News'
    ),
    'livejournal' => array(
      'label' => 'Blogging Platform'
    ),
    'lockerblogger' => array(
      'label' => 'Social Network'
    ),
    'logger2' => array(
      'label' => 'Bookmarking Site'
    ),
    'mymailru' => array(
      'label' => 'Social Network'
    ),
    'markme' => array(
      'label' => 'Bookmarking Site'
    ),
    'mashbord' => array(
      'label' => 'Bookmarking Site'
    ),
    'meinvz' => array(
      'label' => 'Social Network'
    ),
    'mekusharim' => array(
      'label' => 'Social Network'
    ),
    'memonic' => array(
      'label' => 'Other'
    ),
    'memori' => array(
      'label' => 'Bookmarking Site'
    ),
    'meneame' => array(
      'label' => 'Bookmarking Site'
    ),
    'live' => array(
      'label' => 'Bookmarking Site'
    ),
    'mindbodygreen' => array(
      'label' => 'Bookmarking Site'
    ),
    'misterwong' => array(
      'label' => 'Bookmarking Site'
    ),
    'misterwong_de' => array(
      'label' => 'Bookmarking Site'
    ),
    'moemesto' => array(
      'label' => 'Bookmarking Site'
    ),
    'moikrug' => array(
      'label' => 'Social Network'
    ),
    'mototagz' => array(
      'label' => 'Bookmarking Site'
    ),
    'mrcnetworkit' => array(
      'label' => 'Social News'
    ),
    'multiply' => array(
      'label' => 'Social Network'
    ),
    'myaol' => array(
      'label' => 'Bookmarking Site'
    ),
    'myhayastan' => array(
      'label' => 'Social Network'
    ),
    'mylinkvault' => array(
      'label' => 'Bookmarking Site'
    ),
    'n4g' => array(
      'label' => 'Other'
    ),
    'naszaklasa' => array(
      'label' => 'Social Network'
    ),
    'netvibes' => array(
      'label' => 'Social News'
    ),
    'netvouz' => array(
      'label' => 'Bookmarking Site'
    ),
    'newsmeback' => array(
      'label' => 'Social News'
    ),
    'newstrust' => array(
      'label' => 'Social News'
    ),
    'newsvine' => array(
      'label' => 'Social News'
    ),
    'nujij' => array(
      'label' => 'Social News'
    ),
    'odnoklassniki_ru' => array(
      'label' => 'Social Network'
    ),
    'oknotizie' => array(
      'label' => 'Social News'
    ),
    'oneview' => array(
      'label' => 'Bookmarking Site'
    ),
    'orkut' => array(
      'label' => 'Social Network'
    ),
    'dashboard' => array(
      'label' => 'Tools'
    ),
    'oyyla' => array(
      'label' => 'Social News'
    ),
    'packg' => array(
      'label' => 'Bookmarking Site'
    ),
    'pafnetde' => array(
      'label' => 'Social Network'
    ),
    'pdfonline' => array(
      'label' => 'Tools'
    ),
    'pdfmyurl' => array(
      'label' => 'Tools'
    ),
    'phonefavs' => array(
      'label' => 'Bookmarking Site'
    ),
    'pingfm' => array(
      'label' => 'Social Network'
    ),
    'pinterest' => array(
      'label' => 'Social Network'
    ),
    'planypus' => array(
      'label' => 'Other'
    ),
    'plaxo' => array(
      'label' => 'Social Network'
    ),
    'plurk' => array(
      'label' => 'Social Network'
    ),
    'pochvalcz' => array(
      'label' => 'Bookmarking Site'
    ),
    'posteezy' => array(
      'label' => 'Blogging Platform'
    ),
    'posterous' => array(
      'label' => 'Blogging Platform'
    ),
    'pratiba' => array(
      'label' => 'Blogging Platform'
    ),
    'printfriendly' => array(
      'label' => 'Tools'
    ),
    'pusha' => array(
      'label' => 'Bookmarking Site'
    ),
    'qrfin' => array(
      'label' => 'Tools'
    ),
    'quantcast' => array(
      'label' => 'Tools'
    ),
    'qzone' => array(
      'label' => 'Social Network'
    ),
    'readitlater' => array(
      'label' => 'Tools'
    ),
    'reddit' => array(
      'label' => 'Social News'
    ),
    'rediff' => array(
      'label' => 'Social Network'
    ),
    'redkum' => array(
      'label' => 'Bookmarking Site'
    ),
    'ridefix' => array(
      'label' => 'Social News'
    ),
    'scoopat' => array(
      'label' => 'Social News'
    ),
    'scoopit' => array(
      'label' => 'Social News'
    ),
    'sekoman' => array(
      'label' => 'Social Network'
    ),
    'select2gether' => array(
      'label' => 'Social Network'
    ),
    'shaveh' => array(
      'label' => 'Social News'
    ),
    'shetoldme' => array(
      'label' => 'Social News'
    ),
    'sinaweibo' => array(
      'label' => 'Social Network'
    ),
    'skyrock' => array(
      'label' => 'Blogging Platform'
    ),
    'smiru' => array(
      'label' => 'Social News'
    ),
    'sodahead' => array(
      'label' => 'Other'
    ),
    'sonico' => array(
      'label' => 'Social Network'
    ),
    'speedtile' => array(
      'label' => 'Bookmarking Site'
    ),
    'sphinn' => array(
      'label' => 'Social News'
    ),
    'spinsnap' => array(
      'label' => 'Bookmarking Site'
    ),
    'spokentoyou' => array(
      'label' => 'Other'
    ),
    'yiid' => array(
      'label' => 'Social Network'
    ),
    'springpad' => array(
      'label' => 'Bookmarking Site'
    ),
    'squidoo' => array(
      'label' => 'Other'
    ),
    'startaid' => array(
      'label' => 'Bookmarking Site'
    ),
    'startlap' => array(
      'label' => 'Bookmarking Site'
    ),
    'storyfollower' => array(
      'label' => 'Social News'
    ),
    'studivz' => array(
      'label' => 'Social Network'
    ),
    'stuffpit' => array(
      'label' => 'Shopping Site'
    ),
    'stumbleupon' => array(
      'label' => 'Bookmarking Site'
    ),
    'stumpedia' => array(
      'label' => 'Other'
    ),
    'svejo' => array(
      'label' => 'Social News'
    ),
    'symbaloo' => array(
      'label' => 'Other'
    ),
    'taaza' => array(
      'label' => 'Bookmarking Site'
    ),
    'tagza' => array(
      'label' => 'Bookmarking Site'
    ),
    'tarpipe' => array(
      'label' => 'Tools'
    ),
    'thewebblend' => array(
      'label' => 'Bookmarking Site'
    ),
    'thinkfinity' => array(
      'label' => 'Bookmarking Site'
    ),
    'thisnext' => array(
      'label' => 'Shopping Site'
    ),
    'throwpile' => array(
      'label' => 'Blogging Platform'
    ),
    'tipd' => array(
      'label' => 'Social News'
    ),
    'topsitelernet' => array(
      'label' => 'Bookmarking Site'
    ),
    'transferr' => array(
      'label' => 'Other'
    ),
    'tuenti' => array(
      'label' => 'Social Network'
    ),
    'tulinq' => array(
      'label' => 'Social News'
    ),
    'tumblr' => array(
      'label' => 'Blogging Platform'
    ),
    'tusul' => array(
      'label' => 'Bookmarking Site'
    ),
    'tvinx' => array(
      'label' => 'Social News'
    ),
    'tweetmeme' => array(
      'label' => 'Tools'
    ),
    'twitthis' => array(
      'label' => 'Tools'
    ),
    'typepad' => array(
      'label' => 'Blogging Platform'
    ),
    'upnews' => array(
      'label' => 'Social News'
    ),
    'urlaubswerkde' => array(
      'label' => 'Social Network'
    ),
    'urlcapt' => array(
      'label' => 'Tools'
    ),
    'viadeo' => array(
      'label' => 'Social Network'
    ),
    'virb' => array(
      'label' => 'Social Network'
    ),
    'visitezmonsite' => array(
      'label' => 'Bookmarking Site'
    ),
    'vk' => array(
      'label' => 'Social Network'
    ),
    'vkrugudruzei' => array(
      'label' => 'Social Network'
    ),
    'voxopolis' => array(
      'label' => 'Tools'
    ),
    'vybralisme' => array(
      'label' => 'Bookmarking Site'
    ),
    'vyoom' => array(
      'label' => 'Other'
    ),
    'webnews' => array(
      'label' => 'Social News'
    ),
    'webshare' => array(
      'label' => 'Bookmarking Site'
    ),
    'domaintoolswhois' => array(
      'label' => 'Tools'
    ),
    'windows' => array(
      'label' => 'Tools'
    ),
    'windycitizen' => array(
      'label' => 'Social News'
    ),
    'wirefan' => array(
      'label' => 'Bookmarking Site'
    ),
    'wordpress' => array(
      'label' => 'Blogging Platform'
    ),
    'worio' => array(
      'label' => 'Other'
    ),
    'raiseyourvoice' => array(
      'label' => 'Tools'
    ),
    'wykop' => array(
      'label' => 'Bookmarking Site'
    ),
    'xanga' => array(
      'label' => 'Blogging Platform'
    ),
    'xing' => array(
      'label' => 'Social Network'
    ),
    'yahoobkm' => array(
      'label' => 'Bookmarking Site'
    ),
    'yahoomail' => array(
      'label' => 'Email/IM Service'
    ),
    'yammer' => array(
      'label' => 'Blogging Platform'
    ),
    'yardbarker' => array(
      'label' => 'Social Network'
    ),
    'yemle' => array(
      'label' => 'Bookmarking Site'
    ),
    'yigg' => array(
      'label' => 'Social News'
    ),
    'yoolink' => array(
      'label' => 'Bookmarking Site'
    ),
    'yorumcuyum' => array(
      'label' => 'Social News'
    ),
    'youblr' => array(
      'label' => 'Bookmarking Site'
    ),
    'oubookmarks' => array(
      'label' => 'Bookmarking Site'
    ),
    'youmob' => array(
      'label' => 'Bookmarking Site'
    ),
    'yuuby' => array(
      'label' => 'Social Network'
    ),
    'zakladoknet' => array(
      'label' => 'Bookmarking Site'
    ),
    'ziczac' => array(
      'label' => 'Social News'
    ),
    'zingme' => array(
      'label' => 'Social Network'
    ),
  );
}
