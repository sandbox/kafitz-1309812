<?php

define('ACTIVE_REGION', 'enabled');
define('INACTIVE_REGION', 'disabled');

/**
 * @file
 * Stand alone module to handle Addthis button integration
 */

/**
 * Implementation of hook_views_api().
 */
function oneshare_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'oneshare') . '/views',
  );
}

/**
 * Implements hook_init().
 */
function oneshare_init() {
  // Include the start settings for the oneshare module.
  module_load_include('inc', 'oneshare', 'oneshare.settings');
}

/**
 * Implement hook_permission().
 */
function oneshare_permission() {
  return array(
    'administer oneshare' => array(
      'title' => 'Administer Addthis sharing widget',
      'description' => 'Change which services are shown, color, etc and add addthis.com usename',
    ),
    'view oneshare' => array(
      'title' => 'View the addthis widget',
      'description' => 'User can see the button which allows them to share posts',
    ),
  );
}

/**
 * Implement hook_node_view().
 */
function oneshare_node_view($node, $build_mode) {
  if (user_access('view oneshare')) {
    $types = variable_get('oneshare_nodetypes', array());
    if (in_array($node->type, $types)) {
      $node->content['oneshare'] = array(
        '#markup' => oneshare_addthis_widget(),
      );
    }
  }
}

/**
 * Implement of hook_menu().
 */
function oneshare_menu() {
  $items = array();
  $items['admin/config/content/oneshare'] = array(
    'title' => 'One Share',
    'description' => format_string('Manage the look and feel of an !url sharing widget.', array(
      '!url' => l('Addthis', 'http://www.addthis.com/', array('html' => TRUE)),
    )),
    'page callback' => 'oneshare_admin_profile',
    'access arguments' => array('administer oneshare'),
    'file' => 'oneshare.admin.inc',
    'weight' => 10,
  );
  $items['admin/config/content/oneshare/general'] = array(
    'title' => 'General settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_general'),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 1,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/add'] = array(
    'title' => 'Add profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_create'),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 2,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/import'] = array(
    'title' => 'Import profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_import'),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_ACTION,
    'weight' => 3,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/configure'] = array(
    'title' => 'Configure profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_configure', 5),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/delete'] = array(
    'title' => 'Delete profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_delete_confirm', 5),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/clone'] = array(
    'title' => 'Clone profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_clone', 5),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/export'] = array(
    'title' => 'Delete profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_export', 5),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/reset'] = array(
    'title' => 'Reset oneshare',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_reset_confirm', 5),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );
  $items['admin/config/content/oneshare/manage/%/button/%/remove'] = array(
    'title' => 'Reset oneshare',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('oneshare_admin_profile_remove_button_confirm', 5, 7),
    'access arguments' => array('administer oneshare'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
    'file' => 'oneshare.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function oneshare_theme() {
  return array(
    'oneshare-container' => array(
      'template' => 'tpl/oneshare-container',
      'variables' => array(
        'settings' => NULL,
      ),
    ),
    'oneshare-standard' => array(
      'template' => 'tpl/oneshare-standard',
    ),
    'oneshare-toolbox' => array(
      'template' => 'tpl/oneshare-toolbox',
      'variables' => array(
        'settings' => NULL,
      ),
    ),
    'oneshare-toolbox-spec' => array(
      'template' => 'tpl/oneshare-toolbox-spec',
      'variables' => array(
        'settings' => NULL,
      ),
    ),
    'oneshare_admin_customtoolbox' => array(
      'render element' => 'form',
      'file' => 'oneshare.admin.inc',
    ),
    'oneshare_admin_profile_configure' => array(
      'render element' => 'form',
      'file' => 'oneshare.admin.inc',
    ),
  );
}

/**
 * Implements hook_block_info().
 *
 * Provide for each oneshare profile a block.
 */
function oneshare_block_info() {
  $profiles = _oneshare_load_profiles();
  if ($profiles) {
    foreach ($profiles as $key => $profile){
      $blocks['os_' . $profile->type] = array(
        'info' => t('Oneshare ' . $profile->name),
        'cache' => DRUPAL_NO_CACHE,
      );
    }
    return $blocks;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_block_view().
 *
 * Create a view for each oneshare profile block.
 */
function oneshare_block_view($delta = '') {
  if (user_access('view oneshare')) {
    $profiles = _oneshare_load_profiles();
    if ($profiles) {
      $block = array();
      foreach ($profiles as $key => $profile) {
        if ($delta == 'os_' . $profile->type) {
          $block['subject'] = t('Share');
          $block['content'] = oneshare_addthis_widget($profile);
        }
      }
      return $block;
    }
  }
}

/**
 * Returns a visible and usuable profile for the visitor.
 */
function oneshare_addthis_widget($profile) {
  $settings = array();

  // Set the profile id.
  $settings['profileid'] = variable_get('oneshare_profile_id', '');
  if ($settings['profileid']) {
    $settings['profileid'] = '#pubid=' . $settings['profileid'];
  }

  // Add javascript with the profile id.
  drupal_add_js('http://s7.addthis.com/js/250/addthis_widget.js' . $settings['profileid'],
    array('scope' => 'footer')
  );

  // Get the type and use it as an id.
  $settings['widget_id'] = 'oneshare-widget-' . $profile->type;

  // Load the addthis toolbox.
  if ($profile->toolbox) {
    $settings['type'] = 'toolbox';
    $settings['image_size'] = ($profile->size) ? '32x32' : 'default';

    // Load the user configured buttons.
    if ($profile->toolbox_button) {
      $settings['buttons'] = unserialize($profile->buttons);
      $settings['buttons_order'] = unserialize($profile->buttons_order);
      $settings['output'] = theme('oneshare-toolbox-spec', $settings);
    }
    else {
      // Load preferred amount of buttons.
      $settings['pref_amount'] = $profile->amount;
      $settings['output'] = theme('oneshare-toolbox', $settings);
    }
  }
  else {
    // Load the standard addthis button.
    $settings['type'] = 'button';
    $settings['output'] = theme('oneshare-standard');
  }

  // Create the theme.
  return theme('oneshare-container', $settings);
}

/**
 * Returns a clean order array with the used custom toolbox buttons. This is
 * done by ksort the given array and by reindexing the same array.
 */
function _oneshare_sort_reindex_array($array) {
  $result = array();
  ksort($array);
  foreach ($array as $key => $value) {
    $result[] = $array[$key];
  }
  return $result;
}

/**
 *  Returns an array of all used custom toolbox buttons in the selected order.
 */
function _oneshare_toolbox_buttons_order() {
  $order = array();
  foreach (oneshare_toolbox_buttons() as $key => $result) {
    $order[] = $key;
  }
  return $order;
}

/**
 * Return all regions.
 */
function _oneshare_get_regions_array() {
  return array(
    ACTIVE_REGION => ACTIVE_REGION,
    INACTIVE_REGION => INACTIVE_REGION,
  );
}

/**
 * Rearrange the array, recieved from oneshare_newsocialmedia(), in such a way
 * that it is useable for the administrator.
 */
function _oneshare_select_newsocialmedia($social_media) {
  $result = array();
  foreach ($social_media as $key => $value) {
    $result[$value['label']][$key] = $key;
  }
  ksort($result);
  return $result;
}

/**
 * Get all custom buttons who aren't selected yet for the custom toolbox.
 */
function _get_inactive_custom_buttons($toolbox_buttons) {
  $inactive_buttons = oneshare_newsocialmedia();
  foreach ($toolbox_buttons as $rid => $value) {
    if (array_key_exists($value, $inactive_buttons)) {
      unset($inactive_buttons[$value]);
    }
  }
  return $inactive_buttons;
}

/**
 * Load all ore one specific profile.
 */
function _oneshare_load_profiles($name = NULL) {
  $profiles = array();
  if ($name) {
    $profiles = db_query('SELECT * FROM {oneshare} WHERE type = :type', array(':type' => $name))->fetchObject();
  }
  else {
    $result = db_query('SELECT * FROM {oneshare}');
    foreach ($result as $obj) {
      $profiles[] = $obj;
    }
  }
  return (empty($profiles)) ? FALSE : $profiles;
}
