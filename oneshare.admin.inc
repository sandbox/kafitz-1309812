<?php

/**
 * @file
 * Administration settings for One AddThis button integration
 */

/**
 * Overview page off all oneshare profiles. From this page you can do different
 * actions with those profiles.
 */
function oneshare_admin_profile() {
  // Get all profiles.
  $profiles = _oneshare_load_profiles();

  // Start creating the overview table.
  $header = array(t('Profile name'), t('Preview'), array('data' => t('Operations'), 'colspan' => 4));
  $rows = array();

  // If there are profiles, display them all.
  if ($profiles) {
    foreach ($profiles as $key => $profile) {
      $row = array();
      $row[] = $profile->name;
      $row[] = oneshare_addthis_widget($profile);
      $row[] = l('Configure', 'admin/config/content/oneshare/manage/' . $profile->type . '/configure');
      $row[] = l('Delete', 'admin/config/content/oneshare/manage/' . $profile->type . '/delete');
      $row[] = l('Clone', 'admin/config/content/oneshare/manage/' . $profile->type . '/clone');
      $row[] = l('Export', 'admin/config/content/oneshare/manage/' . $profile->type . '/export');
      $rows[] = $row;
    }
  }
  else {
    // There are no profiles. Display a message with a link to the profile
    // creation page.
    $rows[] = array(
      array(
        'colspan' => 4,
        'data' => t('There are currently no profiles. <a href="!url">Add a new profile</a>.', array('!url' => url('admin/config/content/oneshare/add'))),
      )
    );
  }

  // return the table.
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * General settings form().
 *
 * Form for general oneshare settings.
 */
function oneshare_admin_profile_general($form, $form_state) {
  $form = array();

  // Fields.
  $form['profileid'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile Id'),
    '#size' => 25,
    '#description' => t('Add your addthis profile id if you want to have analytics.'),
    '#default_value' => variable_get('oneshare_profile_id'),
  );

  // Submit Button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save general settings'),
  );

  return $form;
}

/**
 * General settings submit form.
 */
function oneshare_admin_profile_general_submit($info, &$form_state) {
  // Store variables.
  variable_set('oneshare_profile_id', $form_state['values']['profileid']);

  // Redirect and display form message.
  $form_state['redirect'] = 'admin/config/content/oneshare';
  drupal_set_message(t('General settings are saved'));
}

/**
 * Create profile form.
 *
 * This form is used for creating a new profile. A name needs to be provided. An
 * Id will automatically be generated.
 */
function oneshare_admin_profile_create() {
  $form = array();

  // Fields.
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#description' => t('The human-readable name of this oneshare profile. This text will be displayed as part of the list on the <em>Oneshare profile</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 29,
    '#disabled' => '',
    '#machine_name' => array(
      'exists' => 'oneshare_admin_profile_load',
    ),
    '#description' => t('A unique machine-readable name for this oneshare profile. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the oneshare profile edit page, in which underscores will be converted into hyphens.'),
  );

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new oneshare profile'),
  );

  return $form;
}

/**
 * This is a callback function of the profile create form. It checks if the
 * machine name is still free to use.
 *
 * Return object if machine name is usuable, FALSE if not.
 */
function oneshare_admin_profile_load($name) {
  return _oneshare_load_profiles($name);
}

/**
 * Submit funtion for oneshare_admin_profile_create().
 *
 * Insert the new data into the database.
 */
function oneshare_admin_profile_create_submit($form, &$form_state) {
  // Insert new profile into the table.
  db_insert('oneshare')
    ->fields(array(
      'type' => $form_state['values']['type'],
      'name' => $form_state['values']['name'],
      'nodes' => serialize(array()),
      'toolbox' => 0,
      'toolbox_button' => 0,
      'amount' => 2,
      'size' => 0,
      'buttons' => serialize(oneshare_toolbox_buttons()),
      'buttons_order' => serialize(_oneshare_toolbox_buttons_order()),
    ))
    ->execute();

  // Redirect and set message.
  $form_state['redirect'] = 'admin/config/content/oneshare/profile';
  drupal_set_message(t('A new oneshare profile is added.'));
}

/**
 * Import profile Form.
 */
function oneshare_admin_profile_import($form, $form_state) {
  $form = array();

  // Fields.
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#description' => t('The human-readable name of this oneshare profile. This text will be displayed as part of the list on the <em>Oneshare profile</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#size' => 30,
  );
  $form['import'] = array(
    '#type' => 'textarea',
    '#title' => 'Import oneshare profile',
    '#rows' => 15,
    '#required' => TRUE,
  );
  $form['update'] = array(
    '#type' => 'checkbox',
    '#title' => 'Override existing profile',
    '#description' => t('If the profile is already present, update the existing profile instead of creating a new one'),
  );

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save oneshare profile'),
  );

  return $form;
}

/**
 * Validate funtion for oneshare_admin_profile_import().
 */
function oneshare_admin_profile_import_validate($form, &$form_state) {
  $import = trim($form_state['values']['import']);

  // Check if given value can be unserialid.
  if (!@unserialize($import)) {
    form_set_error('import', t('Unable to read profile.'));
  }
  else {
    // Can value be inserted into the database.
    $profile = unserialize($import);
    if (!$form_state['values']['update'] && _oneshare_load_profiles($profile['type'])) {
      form_set_error('import', t('Unable to import profile. This profile already exists'));
    }
  }
}

/**
 * Submit function for oneshare_admin_profile_import().
 *
 * Create a new profile through import.
 */
function oneshare_admin_profile_import_submit(&$form, &$form_state) {
  $import = unserialize(trim($form_state['values']['import']));
  $name = ($form_state['values']['name']) ? $form_state['values']['name'] : $import['name'];

  // Type name is not unique and a update is needed. Update existing profile.
  if($form_state['values']['update'] && _oneshare_load_profiles($import['type'])) {
    db_update('oneshare')
      ->fields(array(
          'name' => $name,
          'nodes' => serialize($import['nodes']),
          'toolbox' => $import['toolbox'],
          'toolbox_button' => $import['toolbox_button'],
          'amount' => $import['amount'],
          'size' => $import['size'],
          'buttons' => serialize($import['buttons']),
          'buttons_order' => serialize($import['buttons_order']),
        ))
      ->condition('type', $import['type'])
      ->execute();
    drupal_set_message(t('Profile %profile has been updated', array('%profile' => $name)));
  }
  else {
    // The machine name is unique. So create a new one.
    db_insert('oneshare')
      ->fields(array(
        'type' => $import['type'],
        'name' => $name,
        'nodes' => serialize($import['nodes']),
        'toolbox' => $import['toolbox'],
        'toolbox_button' => $import['toolbox_button'],
        'amount' => $import['amount'],
        'size' => $import['size'],
        'buttons' => serialize($import['buttons']),
        'buttons_order' => serialize($import['buttons_order']),
      ))
      ->execute();
    drupal_set_message(t('Profile %profile has been created', array('%profile' => $name)));
  }

  // Redirect.
  $form_state['redirect'] = 'admin/config/content/oneshare';
}

/**
 * Oneshare admin settings form.
 *
 * Edit the profile.
 */
function oneshare_admin_profile_configure($form, $form_state, $type) {
  // Load profile.
  $profile = _oneshare_load_profiles($type);
  $form = array();

  // Fields.
  $form['#profile'] = $profile;

  // Check if the custom toolbox is enabled.
  if ($profile->toolbox && $profile->toolbox_button) {
    // The custom toolbox button form.
    $form['oneshare_toolbox_overview'] = array(
      '#tree' => TRUE,
    );
    $weight = 0;
    $buttons = unserialize($profile->buttons);
    $buttons_order = unserialize($profile->buttons_order);
    foreach ($buttons_order as $rid => $value) {
      $group = ($buttons[$value]['display']) ? ACTIVE_REGION : INACTIVE_REGION;
      $form['oneshare_toolbox_overview'][$group][$value]['#role'] = (object) array(
        'rid' => 'oneshare_' . $rid,
        'name' => $value,
        'weight' => $weight,
      );
      $form['oneshare_toolbox_overview'][$group][$value]['#weight'] = $weight;
      $form['oneshare_toolbox_overview'][$group][$value]['weight'] = array(
        '#type' => 'textfield',
        '#title' => t('Weight for @title', array('@title' => $value)),
        '#title_display' => 'invisible',
        '#size' => 4,
        '#default_value' => $weight,
        '#attributes' => array('class' => array('order-weight', 'order-weight' . $group)),
      );
      $form['oneshare_toolbox_overview'][$group][$value]['region'] = array(
        '#type' => 'select',
        '#title' => t('Region for @title', array('@title' => $value)),
        '#title_display' => 'invisible',
        '#options' => _oneshare_get_regions_array(),
        '#default_value' => $group,
        '#attributes' => array('class' => array('order-region-select', 'order-region-select-' . $group)),
      );
      $form['oneshare_toolbox_overview'][$group][$value]['label'] = array(
        '#type' => 'textfield',
        '#title' => t('Label'),
        '#size' => 20,
        '#title_display' => 'invisible',
        '#default_value' => $buttons[$value]['label'],
      );
      $weight++;
    }
  }

  // Start vertical tabs.
  $form['oneshare_settings_vertical_tab'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 2,
  );

  // Vertical tab 1: General settings.
  $form['oneshare_general_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General'),
    '#group' => 'oneshare_settings_vertical_tab',
  );
  $form['oneshare_general_settings']['oneshare_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 25,
    '#description' => t('The oneshare profile name.'),
    '#default_value' => $profile->name,
  );
  $form['oneshare_general_settings']['oneshare_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#description' => t('Display an AddThis widget for these node types.'),
    '#default_value' => unserialize($profile->nodes),
    '#options' => node_type_get_names(),
  );

  // Vertical tab 2; Button settings.
  $form['oneshare_button_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Button'),
    '#group' => 'oneshare_settings_vertical_tab',
  );
  $form['oneshare_button_settings']['oneshare_toolbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('AddThis Toolbox'),
    '#description' => t('The AddThis Toolbox utilizes the Client API to create a custom row of sharing service buttons, instead of just one "Share" button.'),
    '#default_value' => $profile->toolbox,
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="oneshare_toolbox"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_size'] = array(
    '#type' => 'checkbox',
    '#title' => t('32x32 Toolbox'),
    '#description' => t('Standard 16x16 Toolbox.'),
    '#default_value' => $profile->size,
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_spec'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create your own sharing menu'),
    '#description' => t('Set up your own sharing menu, If unchecked, random buttons are used.'),
    '#default_value' => $profile->toolbox_button,
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_pref_settings'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="oneshare_toolbox_spec"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_pref_settings']['oneshare_toolbox_pref_amount'] = array(
    '#type' => 'select',
    '#title' => t('Select how many preferenced buttons should be visible.'),
    '#description' => t('Select an amount of buttons'),
    '#default_value' => $profile->amount,
    '#options' => drupal_map_assoc(range(1, 11)),
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_custom_buttons'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="oneshare_toolbox_spec"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['oneshare_button_settings']['oneshare_toolbox_settings']['oneshare_toolbox_custom_buttons']['oneshare_add_buttons_list'] = array(
    '#type' => 'select',
    '#title' => t('Select a custom toolbox button.'),
    '#multiple' => TRUE,
    '#description' => t('You can add multiple buttons to the custom toolbox.'),
    '#options' => _oneshare_select_newsocialmedia(_get_inactive_custom_buttons(unserialize($profile->buttons_order))),
    '#size' => 12,
  );

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );

  $form['actions']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset !profile', array('!profile' => $profile->name)),
    '#submit' => array('oneshare_admin_profile_reset'),
  );

  return $form;
}

/**
 * Submit function for oneshare_admin_profile_configure();
 *
 * Save the new configuration of the profile.
 */
function oneshare_admin_profile_configure_submit($form, &$form_state) {
  // Load profile.
  $profile = $form['#profile'];
  $buttons = unserialize($profile->buttons);
  $buttons_order = unserialize($profile->buttons_order);

  // Safe the specific button settings.
  // Check if the custom toolbox is enabled and and the form var
  // 'oneshare_toolbox_spec' is checked.
  if ($form_state['values']['oneshare_toolbox_spec'] && $profile->toolbox && $profile->toolbox_button) {
    $buttons = $buttons_order = array();
    foreach (unserialize($profile->buttons) as $key => $value) {

      // Get the correct button.
      if (array_key_exists(ACTIVE_REGION, $form_state['values']['oneshare_toolbox_overview']) && array_key_exists($key, $form_state['values']['oneshare_toolbox_overview'][ACTIVE_REGION])) {
        $button = $form_state['values']['oneshare_toolbox_overview'][ACTIVE_REGION][$key];
      }
      else {
        $button = $form_state['values']['oneshare_toolbox_overview'][INACTIVE_REGION][$key];
      }

      $buttons_order[$button['weight']] = $key;
      $buttons[$key] = array(
        'label' => $value['label'],
        'display' => ($button['region'] == ACTIVE_REGION) ? TRUE : FALSE,
        'del' => array_key_exists($key, oneshare_newsocialmedia()),
      );
    }

    // Get additional buttons.
    foreach ($form_state['values']['oneshare_add_buttons_list'] as $key => $value) {
      $buttons[$value] = array(
        'label' => '',
        'display' => TRUE,
        'del' => TRUE,
      );
      $buttons_order[] = $value;
    }
  }
  else if ($form_state['values']['oneshare_toolbox'] && $form_state['values']['oneshare_toolbox_spec']) {
    // The custom toolbox is selected as a new configuration. We only need the
    // selected buttons.
    // Get additional buttons.
    foreach ($form_state['values']['oneshare_add_buttons_list'] as $key => $value) {
      $buttons[$value] = array(
        'label' => '',
        'display' => TRUE,
        'del' => TRUE,
      );
      $buttons_order[] = $value;
    }
  }

  // Get all checked nodetypes.
  $nodetypes = array();
  foreach ($form_state['values']['oneshare_nodetypes'] as $key => $value) {
    if ($value) {
      $nodetypes[] = $value;
    }
  }

  // Update profile
  db_update('oneshare')
    ->fields(array(
        'name' => $form_state['values']['oneshare_name'],
        'nodes' => serialize($nodetypes),
        'toolbox' => $form_state['values']['oneshare_toolbox'],
        'toolbox_button' => $form_state['values']['oneshare_toolbox_spec'],
        'amount' => $form_state['values']['oneshare_toolbox_pref_amount'],
        'size' => $form_state['values']['oneshare_toolbox_size'],
        'buttons' => serialize($buttons),
        'buttons_order' => serialize(_oneshare_sort_reindex_array($buttons_order)),
      ))
    ->condition('type', $profile->type)
    ->execute();

  drupal_set_message(t('Form is saved'));
}

/**
 * Redirection to the profile reset funtion.
 */
function oneshare_admin_profile_reset($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/content/oneshare/manage/' . $form['#profile']->type . '/reset';
}

/**
 * Theming funtion for oneshare_admin_profile_configure().
 *
 * Theming function, needed for the info on the vertical tabs and for having
 * the tabledrag functionality on the custom toolbox form. So the correct
 * position for the buttons can be selected.
 */
function theme_oneshare_admin_profile_configure($variables) {
  $output = '';
  $profile = $variables['form']['#profile'];

  // Add js file for controlling the info on the vertical tabs.
  drupal_add_js(drupal_get_path('module', 'oneshare') . '/js/oneshare_vertical_tab.js');

  // Check if cusstom toolbos is selected, if so, create the table with drag
  // function.
  if ($profile->toolbox && $profile->toolbox_button) {
    // Add additional js and css files.
    drupal_add_js('misc/tableheader.js');
    drupal_add_js(drupal_get_path('module', 'oneshare') . '/js/oneshare_drag.js');
    drupal_add_css(drupal_get_path('module', 'oneshare') . '/css/oneshare_form.css');

    // Declare important variables.
    $toolbox = unserialize($profile->buttons);
    $newsocialmedia = oneshare_newsocialmedia();
    $removable = array();
    $form = $variables['form'];

    // Start creating the table
    $header = array(t('Name'), t('weight'), t('Region'), t('Label'), t('Operations'));

    // Get all buttons that are allowed to be deleted.
    foreach ($toolbox as $key => $value) {
      if (array_key_exists('del', $value) && $value['del']) {
        $removable[$key] = $key;
      }
    }

    // Loop through each region.
    foreach (_oneshare_get_regions_array() as $group) {
      // Add tabledrag to all groups and regions.
      drupal_add_tabledrag('oneshare', 'match', 'sibling', 'order-region-select', 'order-region-select-' . $group, NULL, FALSE);
      drupal_add_tabledrag('oneshare', 'order', 'sibling', 'order-weight', 'order-weight' . $group);

      // Region row title.
      $rows[] = array('data' => array('#attributes' => array('data' => $group, 'colspan' => 4)), 'class' => array('region-title', 'region-title-' . $group));

      // Region empty message.
      $message_class = array('region-message', 'region-' . $group . '-message');

      // Are there values for the specific region?
      if (array_key_exists($group, $form['oneshare_toolbox_overview'])) {
        $message_class[] = 'region-not-empty';
        $empty = FALSE;
      }
      else {
        $message_class[] = 'region-empty';
        $empty = TRUE;
      }

      // Fill all the data rows.
      $rows[] = array('data' => array('#attributes' => array('data' => '<em>There are no items in this region</em>', 'colspan' => 4)), 'class' => $message_class);
      if (!$empty) {
        foreach (element_children($form['oneshare_toolbox_overview'][$group]) as $rid) {
          $row = array();
          $row[] = check_plain($form['oneshare_toolbox_overview'][$group][$rid]['#role']->name);
          $row[] = drupal_render($form['oneshare_toolbox_overview'][$group][$rid]['weight']);
          $row[] = drupal_render($form['oneshare_toolbox_overview'][$group][$rid]['region']);
          $row[] = drupal_render($form['oneshare_toolbox_overview'][$group][$rid]['label']);
          if (array_key_exists($rid, $removable)) {
            $row[] = l(t('Remove button'), 'admin/config/content/oneshare/manage/' . $profile->type . '/button/' . $rid . '/remove');
          }
          else {
            $row[] = '';
          }
          $rows[] = array('data' => $row, 'class' => array('draggable'));
        }
      }
    }

    // Theme the table and render the form.
    $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'oneshare')));
    $output .= drupal_render_children($form);
  }

  return $output;
}

/**
 * Delete a profile confirmation function.
 */
function oneshare_admin_profile_delete_confirm($form, $form_state, $type) {
  $profile = _oneshare_load_profiles($type);
  $form['#profile'] = array('#type' => 'hidden', '#value' => $profile);
  return confirm_form($form, t('Are you sure you want to delete %name as a oneshare profile?', array('%name' => $profile->name)), 'admin/config/content/oneshare/profile');
}

/**
 * Submit handler for oneshare_admin_profile_delete_confirm().
 */
function oneshare_admin_profile_delete_confirm_submit($form, &$form_state) {
  db_delete('oneshare')->condition('type', $form['#profile']['#value']->type)->execute();

  // redirect and set message.
  $form_state['redirect'] = 'admin/config/content/oneshare';
  drupal_set_message(t('Profile %profile deleted', array('%profile' => $form['#profile']['#value']->name)));

  return;
}

/**
 * Clone an existing profile.
 */
function oneshare_admin_profile_clone($form, $form_state, $type) {
  $profile = _oneshare_load_profiles($type);
  $form = array();

  // Fields.
  $form['#profile'] = $profile;
  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#description' => t('The human-readable name of this oneshare profile. This text will be displayed as part of the list on the <em>Oneshare profile</em> page. It is recommended that this name begin with a capital letter and contain only letters, numbers, and spaces. This name must be unique.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 29,
    '#disabled' => '',
    '#machine_name' => array(
      'exists' => 'oneshare_admin_profile_load',
    ),
    '#description' => t('A unique machine-readable name for this oneshare profile. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the oneshare profile edit page, in which underscores will be converted into hyphens.'),
  );

  // Submit button.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clone !profile', array('!profile' => $profile->name)),
  );

  return $form;
}

/**
 * Submit funtion for oneshare_admin_profile_clone().
 */
function oneshare_admin_profile_clone_submit($form, &$form_state) {
  // Insert new profile.
  db_insert('oneshare')
    ->fields(array(
      'type' => $form_state['values']['type'],
      'name' => $form_state['values']['name'],
      'nodes' => $form['#profile']->nodes,
      'toolbox' => $form['#profile']->toolbox,
      'toolbox_button' => $form['#profile']->toolbox_button,
      'amount' => $form['#profile']->amount,
      'size' => $form['#profile']->size,
      'buttons' => $form['#profile']->buttons,
      'buttons_order' => $form['#profile']->buttons_order,
    ))
    ->execute();

  // Redirect and set message.
  $form_state['redirect'] = 'admin/config/content/oneshare';
  drupal_set_message(t('A new oneshare profile is created.'));
}

/**
 * Form function for exporting a profile.
 */
function oneshare_admin_profile_export($form, $form_state, $type) {
  $profile = _oneshare_load_profiles($type);

  // Put all variables into one array.
  $export['type'] = $profile->type;
  $export['name'] = $profile->name;
  $export['nodes'] = unserialize($profile->nodes);
  $export['toolbox'] = $profile->toolbox;
  $export['toolbox_button'] = $profile->toolbox_button;
  $export['amount'] = $profile->amount;
  $export['size'] = $profile->size;
  $export['buttons'] = unserialize($profile->buttons);
  $export['buttons_order'] = unserialize($profile->buttons_order);

  // Fields.
  $form['export'] = array(
    '#type' => 'textarea',
    '#title' => t('Oneshare profile export'),
    '#default_value' => serialize($export),
    '#rows' => 15,
  );

  return $form;
}

/**
 * Reset oneshare confirm function.
 */
function oneshare_admin_profile_reset_confirm($form, $form_state, $type) {
  $profile = _oneshare_load_profiles($type);
  $form['#profile'] = array('#type' => 'hidden', '#value' => $profile);
  return confirm_form($form, t('Are you sure you want to reset %profile', array('%profile' => $profile->name)), 'admin/config/content/oneshare/manage/' . $type . '/configure');
}

/**
 * Reset profile confirmation function.
 */
function oneshare_admin_profile_reset_confirm_submit($form, &$form_state) {
  // Update the profile to original default settings.
  db_update('oneshare')
    ->fields(array(
      'nodes' => serialize(array()),
      'toolbox' => 0,
      'toolbox_button' => 0,
      'amount' => 2,
      'size' => 0,
      'buttons' => serialize(oneshare_toolbox_buttons()),
      'buttons_order' => serialize(_oneshare_toolbox_buttons_order()),
    ))
    ->condition('type', $form['#profile']['#value']->type)
    ->execute();

  // redirect and set message.
  $form_state['redirect'] = 'admin/config/content/oneshare/manage/' . $form['#profile']['#value']->type . '/configure';
  drupal_set_message(t('Profile %profile has been reset to original values', array('%profile' => $form['#profile']['#value']->name)));
  return;
}

/**
 * Remove a custom toolbox button confirm function.
 */
function oneshare_admin_profile_remove_button_confirm($form, $form_state, $type, $button) {
  $profile = _oneshare_load_profiles($type);
  $form['#profile']['profile'] = array('#type' => 'hidden', '#value' => $profile);
  $form['#profile']['button'] = array('#type' => 'hidden', '#value' => $button);
  return confirm_form($form, t('Are you sure you want to remove %button from %profile', array('%button' => $button, '%profile' => $profile->name)), 'admin/config/content/oneshare/manage/' . $type . '/configure');
}

/**
 * Submit for oneshare_admin_profile_remove_button().
 */
function oneshare_admin_profile_remove_button_confirm_submit($form, &$form_state) {
  // Set impoortan variables.
  $profile = $form['#profile']['profile']['#value'];
  $buttons = unserialize($profile->buttons);
  $buttons_order = unserialize($profile->buttons_order);
  $button = $form['#profile']['button']['#value'];

  // Check if button can be deleted.
  if ($buttons[$button]['del']) {
    unset($buttons[$button]);
    foreach ($buttons_order as $key => $value) {
      if ($value == $button) {
        unset($buttons_order[$key]);
      }
    }

    // Update profile.
    db_update('oneshare')
      ->fields(array(
        'buttons' => serialize($buttons),
        'buttons_order' => serialize($buttons_order),
      ))
      ->condition('type', $profile->type)
      ->execute();

    //Set message.
    drupal_set_message(t('Button %name has been removed from %profile.', array('%name' => $button, '%profile' => $profile->name)));
  }

  // redirect.
  $form_state['redirect'] = 'admin/config/content/oneshare/manage/' . $profile->type . '/configure';
  return;
}
