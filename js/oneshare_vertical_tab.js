(function ($) {

/**
 * Provide the summary information for the block settings vertical tabs.
 */
Drupal.behaviors.oneshareSettingsSummary = {
  attach: function (context) {
    // The drupalSetSummary method required for this behavior is not available
    // on the Blocks administration page, so we need to make sure this
    // behavior is processed only if drupalSetSummary is defined.
    if (typeof jQuery.fn.drupalSetSummary == 'undefined') {
      return;
    }

    // Display information for the first tab.
    $('fieldset#edit-oneshare-general-settings', context).drupalSetSummary(function (context) {
      var vals = [];
      if($('input#edit-oneshare-profileid').val()) {
        vals.push('UA-code: ' + $('input#edit-oneshare-profileid').val());
      }
      $('#edit-oneshare-nodetypes input:checked').each(function(index){
        if (index == 0) {
          vals.push('Content types: ' + $(this).val());
        }
        else {
          vals.push($(this).val());
        }
      });
      return vals.join(', ');
    });

    // Display information for the second tab.
    $('fieldset#edit-oneshare-button-settings', context).drupalSetSummary(function (context) {
      var vals = [];
      if($('input#edit-oneshare-toolbox').attr('checked')) {
        vals.push('Addthis toolbox');
        if($('input#edit-oneshare-toolbox-size').attr('checked')) {
          vals.push('Size: 32x32');
        }
        else {
          vals.push('Size: 16x16');
        }
        if($('input#edit-oneshare-toolbox-spec').attr('checked')) {
          vals.push('Type: custom');
        }
        else {
          vals.push('Type: random');
          vals.push('Amount: ' + $('select#edit-oneshare-toolbox-pref-amount').val());
        }
      }
      else {
        vals.push('Share button');
      }
      return vals.join(', ');
    });
  }
};
})(jQuery);

